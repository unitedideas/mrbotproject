package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "os"
    "time"

    "github.com/slack-go/slack"
)

func main() {
    api := slack.New("xoxb-2561958232453-2585390147971-uJ7emQx54HKUicmgCTDO2dFb")

    MRList := getMRBymrbotlabelLabel()
    if MRList == nil {
        fmt.Println("fail to getMRBymrbotlabelLabel")
        os.Exit(3)
    }

    for _, item := range *MRList {
        channelID, timestamp, err := api.PostMessage(
            "C02GHU67U5T",
            slack.MsgOptionText(item.Title+": "+item.WebUrl, false),
        )

        if err != nil {
            fmt.Printf("%s\n", err)
            return
        }
        fmt.Printf("Message successfully sent to channel %s at %s", channelID, timestamp)
    }
}

type getMRBymrbotlabelLabelResponse []getMRBymrbotlabelLabelStruct

func getMRBymrbotlabelLabel() *getMRBymrbotlabelLabelResponse {
    url := "https://gitlab.com/api/v4/merge_requests?labels=mrbotlabel"
    method := "GET"

    client := &http.Client{}
    req, err := http.NewRequest(method, url, nil)

    if err != nil {
        fmt.Println(err)
        return nil
    }
    req.Header.Add("Authorization", "Bearer b8PzW_18SixWyRwb-2e2")
    //req.Header.Add("Cookie", "_gitlab_session=33581a79831b6468bd250f1491a2d316; experimentation_subject_id=eyJfcmFpbHMiOnsibWVzc2FnZSI6IklqZG1aRGN5WW1VMExUUXhOemN0TkRkaE5pMDROVGM1TFRVM1lXTTFObU5pTjJVME9DST0iLCJleHAiOm51bGwsInB1ciI6ImNvb2tpZS5leHBlcmltZW50YXRpb25fc3ViamVjdF9pZCJ9fQ%3D%3D--af6d487d99d0c55086d31c8df243539bbcdda727")

    res, err := client.Do(req)
    if err != nil {
        fmt.Println(err)
        return nil
    }
    defer res.Body.Close()

    body, err := ioutil.ReadAll(res.Body)
    if err != nil {
        fmt.Println(err)
        return nil
    }

    var response getMRBymrbotlabelLabelResponse

    if err := json.Unmarshal(body, &response); err != nil {
        fmt.Println(err)
        return nil
    }

    return &response
}

type getMRBymrbotlabelLabelStruct struct {
    Id             int         `json:"id"`
    Iid            int         `json:"iid"`
    ProjectId      int         `json:"project_id"`
    Title          string      `json:"title"`
    Description    string      `json:"description"`
    State          string      `json:"state"`
    CreatedAt      time.Time   `json:"created_at"`
    UpdatedAt      time.Time   `json:"updated_at"`
    MergedBy       interface{} `json:"merged_by"`
    MergedAt       interface{} `json:"merged_at"`
    ClosedBy       interface{} `json:"closed_by"`
    ClosedAt       interface{} `json:"closed_at"`
    TargetBranch   string      `json:"target_branch"`
    SourceBranch   string      `json:"source_branch"`
    UserNotesCount int         `json:"user_notes_count"`
    Upvotes        int         `json:"upvotes"`
    Downvotes      int         `json:"downvotes"`
    Author         struct {
        Id        int    `json:"id"`
        Name      string `json:"name"`
        Username  string `json:"username"`
        State     string `json:"state"`
        AvatarUrl string `json:"avatar_url"`
        WebUrl    string `json:"web_url"`
    } `json:"author"`
    Assignees                 []interface{} `json:"assignees"`
    Assignee                  interface{}   `json:"assignee"`
    Reviewers                 []interface{} `json:"reviewers"`
    SourceProjectId           int           `json:"source_project_id"`
    TargetProjectId           int           `json:"target_project_id"`
    Labels                    []string      `json:"labels"`
    Draft                     bool          `json:"draft"`
    WorkInProgress            bool          `json:"work_in_progress"`
    Milestone                 interface{}   `json:"milestone"`
    MergeWhenPipelineSucceeds bool          `json:"merge_when_pipeline_succeeds"`
    MergeStatus               string        `json:"merge_status"`
    Sha                       string        `json:"sha"`
    MergeCommitSha            interface{}   `json:"merge_commit_sha"`
    SquashCommitSha           interface{}   `json:"squash_commit_sha"`
    DiscussionLocked          interface{}   `json:"discussion_locked"`
    ShouldRemoveSourceBranch  interface{}   `json:"should_remove_source_branch"`
    ForceRemoveSourceBranch   bool          `json:"force_remove_source_branch"`
    Reference                 string        `json:"reference"`
    References                struct {
        Short    string `json:"short"`
        Relative string `json:"relative"`
        Full     string `json:"full"`
    } `json:"references"`
    WebUrl    string `json:"web_url"`
    TimeStats struct {
        TimeEstimate        int         `json:"time_estimate"`
        TotalTimeSpent      int         `json:"total_time_spent"`
        HumanTimeEstimate   interface{} `json:"human_time_estimate"`
        HumanTotalTimeSpent interface{} `json:"human_total_time_spent"`
    } `json:"time_stats"`
    Squash               bool `json:"squash"`
    TaskCompletionStatus struct {
        Count          int `json:"count"`
        CompletedCount int `json:"completed_count"`
    } `json:"task_completion_status"`
    HasConflicts                bool        `json:"has_conflicts"`
    BlockingDiscussionsResolved bool        `json:"blocking_discussions_resolved"`
    ApprovalsBeforeMerge        interface{} `json:"approvals_before_merge"`
}
